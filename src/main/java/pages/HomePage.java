package pages;

import wrappers.Annotations;

public class HomePage extends Annotations {
	
	public HomePage verifyLoginName() {
		String loginName = driver.findElementByTagName("h2").getText();
		if(loginName.contains("DemoSalesManager")) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
//	public LoginPage clickLogoutButton() {
//		driver.findElementByClassName("decorativeSubmit").click();
//		return new LoginPage();
//	}
	
	public MyHomePage clickCRMSFA(){
		driver.findElementByLinkText("CRM/SFA").click();
		return new MyHomePage();
	}
	
	
	
	
	
	
	

}
