package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations {
	
	public static String Fname1;
	
	public CreateLeadPage EnterCompanyName(String CName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(CName);
		return this;
		
	}
	
	public CreateLeadPage EnterFirstNameCL(String Fname2) {
		driver.findElementById("createLeadForm_firstName").sendKeys(Fname2);
		Fname1 = driver.findElementById("createLeadForm_firstName").getAttribute("value");
		return this;
	}
	
	public CreateLeadPage EnterLastName(String LName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(LName);
		return this;
	}
	
	public ViewLeadPage ClickCreateLeadButton() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}
}
