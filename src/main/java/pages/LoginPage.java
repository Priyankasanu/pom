package pages;

import wrappers.Annotations;

public class LoginPage extends Annotations {
	
	public LoginPage enterUserName(String Data) {
		driver.findElementById("username").sendKeys(Data);
		return this;
	}
	
	public LoginPage enterPassword(String Data) {
		driver.findElementById("password").sendKeys(Data);
		return this;
	}
	
	public HomePage clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
		/*HomePage hp = new HomePage();
		return hp;*/
		return new HomePage();
	}
	
	
	
	
	

}
