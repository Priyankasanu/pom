package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import wrappers.Annotations;

public class MyLeadsPage extends Annotations{
	
	public CreateLeadPage ClickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
	
	public FindLeadsPage ClickFindLeads() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}
	
	public FindLeadsPage clickFindLeads() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}
	
	public OpenTapsCRMPage clickMergeLeads() {
		driver.findElementByLinkText("Merge Leads").click();
		return new OpenTapsCRMPage();
	}
	
	
}
