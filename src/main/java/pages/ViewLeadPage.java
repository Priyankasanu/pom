package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName() {
		String FName = driver.findElementById("viewLead_firstName_sp").getText();
		if(FName.equals(CreateLeadPage.Fname1)) {
			System.out.println("The First Name is Expected");
		} else {
			System.out.println("Its not Expected");
		}
		
		return this;
	}
	
	public OpenTapsCRMPage ClickEdit() {
		driver.findElementByLinkText("Edit").click();
		return new OpenTapsCRMPage();
	}
	
	public void verifyCompanyName() {
		String CompName = driver.findElementById("viewLead_companyName_sp").getText();
		if(CompName.contains(OpenTapsCRMPage.UpdatedCompanyName)) {
			System.out.println("Verified");
			
		} else {
			System.out.println("Not Expected");
		}
		
	}
	
	public MyLeadsPage clickDeleteButton() {
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	
	public OpenTapsCRMPage clickDuplicateButton() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new OpenTapsCRMPage();
	}

}
