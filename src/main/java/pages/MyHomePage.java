package pages;

import wrappers.Annotations;

public class MyHomePage extends Annotations{
	
	public MyLeadsPage ClickLead() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}

}
