package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

import wrappers.Annotations;

public class OpenTapsCRMPage extends Annotations {
	
	public static String UpdatedCompanyName;
	public static String NewLeadID;
	
	public OpenTapsCRMPage EditCompanyName() {
		WebElement CompanyName = driver.findElementById("updateLeadForm_companyName");
		CompanyName.sendKeys("TestLeaf");
		UpdatedCompanyName = CompanyName.getAttribute("value");
		
		return this;
	}
	
	public ViewLeadPage ClickUpdate() {
		driver.findElementByXPath("//input[@value='Update']").click();
		return new ViewLeadPage();
	}
	
	public ViewLeadPage clickCreateLead() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return new ViewLeadPage();
	}

	public void verifyLeadName() {
		String NewLeadID = driver.findElementById("viewLead_firstName_sp").getText();
		if(NewLeadID.contains(FindLeadsPage.OldLeadname)) {
			System.out.println("Matched");
		} else {
			System.out.println("Not Matched");
		}
	}
	
	public OpenTapsCRMPage clickLookup1() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		return this;
	}
	
	public OpenTapsCRMPage clickLookup2() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		return this;
	}
	
	public FindLeadsPage controlFirstWindow() {
		Set<String> windowHandle = driver.getWindowHandles();
		List<String> window = new ArrayList<String>();
		window.addAll(windowHandle);
		String windowname = window.get(0);
		driver.switchTo().window(windowname);
		return new FindLeadsPage();
	}
	
	public ViewLeadPage clickMergeButton() {
		driver.findElementByLinkText("Merge").click();
		return new ViewLeadPage();
	}
	
	public FindLeadsPage controlSecondWindow() {
		Set<String> windowHandle = driver.getWindowHandles();
		List<String> window = new ArrayList<String>();
		window.addAll(windowHandle);
		String windowname = window.get(1);
		driver.switchTo().window(windowname);
		return new FindLeadsPage();
	}
}
