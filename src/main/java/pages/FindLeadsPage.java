package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import wrappers.Annotations;

public class FindLeadsPage extends Annotations{
	
	public static String text;
	public static String OldLeadname;
	
	public FindLeadsPage EnterFirstName() {
		
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("priyanka");
		return this;
		
	}
	
	public FindLeadsPage ClickFindLeadsButton() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	public FindLeadsPage Wait() throws InterruptedException {
		Thread.sleep(3000);
		return this;
	}
	public ViewLeadPage ClickFirstID() {
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		return new ViewLeadPage();
		
	}
	
	public FindLeadsPage clickPhone() {
		driver.findElementByXPath("//span[text()='Phone']").click();
		return this;
	}
	
	public FindLeadsPage enterPhoneNumber() {
		driver.findElementByName("phoneNumber").sendKeys("9600047889");
		return this;
	}
	
	public FindLeadsPage captureLeadID() {
		WebElement LeadID = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		text = LeadID.getText();
		return this;
	}
	
	public FindLeadsPage enterLeadID() {
		driver.findElementByName("id").sendKeys(text);
		return this;
	}
	
	public FindLeadsPage clickEmail() {
		driver.findElementByXPath("//span[text()='Email']").click();
		return this;
	}
	
	public FindLeadsPage enterEmailID() {
		driver.findElementByName("emailAddress").sendKeys("priyankasanu000@gmail.com");
		return this;
	}
	
	public FindLeadsPage captureFirstName() {
		 OldLeadname = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]").getText();
		 return this;
	}
	
	
	public FindLeadsPage enterFirstnameLookup() {
		driver.findElementByName("firstName").sendKeys("Priyanka");
		return this;
	}
	
	public OpenTapsCRMPage ClickFirstIDLookup() {
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		return new OpenTapsCRMPage();
	}
	
	
	
	public OpenTapsCRMPage ClickSecondIDLookup() {
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]").click();
		return new OpenTapsCRMPage();
		 
	}
}
