package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC_002";
	}
	
	@Test(dataProvider = "fetchData")
	public void EditLeadMethod(String Username1, String Password1) throws InterruptedException {
		new LoginPage()
		.enterUserName(Username1)
		.enterPassword(Password1)
		.clickLoginButton()
		.clickCRMSFA()
		.ClickLead()
		.ClickFindLeads()
		.EnterFirstName()
		.ClickFindLeadsButton()
		.Wait()
		.ClickFirstID()
		.ClickEdit()
		.EditCompanyName()
		.ClickUpdate()
		.verifyCompanyName();
		
	}	

}
