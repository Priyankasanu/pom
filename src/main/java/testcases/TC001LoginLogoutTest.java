package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC001LoginLogoutTest extends Annotations {
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC001";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLeadMethod(String UserName, String PassWord,String CompanyName, String FirstName, String LastName) {
		new LoginPage()
		.enterUserName(UserName)
		.enterPassword(PassWord)
		.clickLoginButton()
		.verifyLoginName()
		.clickCRMSFA()
		.ClickLead()
		.ClickCreateLead()
		.EnterCompanyName(CompanyName)
		.EnterFirstNameCL(FirstName)
		.EnterLastName(LastName)
		.ClickCreateLeadButton()
		.verifyFirstName();
	}
	
	
	
	

	
}
