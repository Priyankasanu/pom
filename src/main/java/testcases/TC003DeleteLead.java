package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC003DeleteLead extends Annotations{
	@BeforeTest
	public void setData() {
		excelFileName = "TC_002";
	}
	
	@Test(dataProvider = "fetchData")
	public void DeleteLead(String Username, String Password) throws InterruptedException {
		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.clickCRMSFA()
		.ClickLead()
		.ClickFindLeads()
		.clickPhone()
		.enterPhoneNumber()
		.ClickFindLeadsButton()
		.Wait()
		.captureLeadID()
		.ClickFirstID()
		.clickDeleteButton()
		.ClickFindLeads()
		.enterLeadID()
		.ClickFindLeadsButton();
	}

}
