package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC004DuplicateLead extends Annotations {
	@BeforeTest
	public void setData() {
		excelFileName="TC_004";
	}
	@Test(dataProvider = "fetchData")
	public void DuplicateLead(String UserName4, String Password4) throws InterruptedException {
		new LoginPage()
		.enterUserName(UserName4)
		.enterPassword(Password4)
		.clickLoginButton()
		.clickCRMSFA()
		.ClickLead()
		.clickFindLeads()
		.clickEmail()
		.enterEmailID()
		.ClickFindLeadsButton()
		.Wait()
		.captureFirstName()
		.ClickFirstID()
		.clickDuplicateButton()
		.clickCreateLead()
		.verifyFirstName();
		
		
		
	}
	

}
